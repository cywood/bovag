# Voertuigen

## 🎁 Online demo
[Bekijk hier de online demo](https://bovag.stolk.tech/)

## 👨‍💻 Voorbereiding

Zorg er voor dat je **Node.js** versie **8** of hoger geïnstalleerd hebt.

## 🔨 De app installeren

De app kan geinstalleerd worden via `yarn` of `npm i`

## 🚀 De app opstarten

De app is te runnen middels `yarn start` of `npm start` - De Vehicle API is nodig om de voertuigen op te halen

## 🚙 Backend mock server

De voertuigen op viaBOVAG.nl worden aangeleverd middels de *Vehicle API*. Deze bevindt zich in de map `/api`. We starten de mock server als volgt:

```bash
$ cd api
$ npm i && npm start
```

