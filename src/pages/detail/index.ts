import { getCarById } from '../../api/cars';
import { generateCarDetailBlock } from '../../components/cardetail';
import { ICar } from '../../interfaces';
import PageBase from '../page-base';

export default class extends PageBase {
  public car: ICar; 

  constructor(params) {
    super(params);
  }

  async render() {
    return `
      <div class="details">
        <a href="/" class="btn">Terug naar overzicht</a>
        <div id="car" class="car-detail"></div>
      </div>
    `;
  }
  async afterRender() {
    this.car = await getCarById(this.params.id);
    const carsElement = document.querySelector('#car');
    
    if (this.car) {
      this.setTitle(`Details ${this.car.brand} ${this.car.model}`);
      const block = generateCarDetailBlock(this.car);
      carsElement.appendChild(block);
    }
  }
}