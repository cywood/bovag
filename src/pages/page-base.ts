export default class {
  params: any;
  constructor(params) {
    this.params = params;
  }

  setTitle(title) {
    document.title = title;
  }

  async render() {
    return '';
  }

  async afterRender() {
    return;
  }
}