import PageBase from '../page-base';

import { getAllCars } from '../../api/cars';
import { ICar } from '../../interfaces';
import { generateCarBlock } from '../../components/carblock';
import './index.scss'

export default class extends PageBase {
  cars: ICar[] = [];
  constructor(params) {
    super(params);
    this.setTitle('Car Overview - Home');
  }

  async render() {
    return `
      <div class="home">
        <div id="cars" class="cars"></div>
      </div>
    `;
  }
  
  async afterRender() {
    this.cars = await getAllCars();
    const carsElement = document.querySelector('#cars');
    if (this.cars && this.cars.length) {
      this.cars.map((car: ICar) => {
        const block = generateCarBlock(car);
        carsElement.appendChild(block);
      })
    }
  }
}