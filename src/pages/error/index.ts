import PageBase from '../page-base';
import './index.scss';

export default class extends PageBase {
  constructor(params) {
    super(params);
    this.setTitle('404');
  }

  async render() {
    return `
      <div class="error">
        <h1>404</h1>
        <a href="/" class="btn">Terug naar overzicht</a>
      </div>
    `;
  }
  async afterRender() {
  }
}