const apiUrl = 'http://localhost:3000'

const headers = {
  'Content-Type': 'application/json'
}

export const getAllCars = async () => {
  const result = await fetch(`${apiUrl}/vehicles`, {
    headers
  });
  const carsList = await result.json();
  return carsList;
}

export const getCarById = async (id: string) => {
  const result = await fetch(`${apiUrl}/vehicles/${id}`, {
    headers
  });
  const carDetail = await result.json();
  return carDetail;
}