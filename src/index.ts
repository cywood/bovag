"use strict";
import router from './router';

window.addEventListener('popstate', router);

document.addEventListener('DOMContentLoaded', () => {
  router();
});