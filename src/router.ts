import Home from './pages/home/';
import CarDetail from './pages/detail/';
import ErrorPage from './pages/error';
import { IRouter } from './interfaces';

const pathToRegex = path => new RegExp("^" + path.replace(/\//g, "\\/").replace(/:\w+/g, "(.+)") + "$");

const getParams = match => {
  const values = match.result.slice(1);
  const keys = Array.from(match.route.path.matchAll(/:(\w+)/g)).map(r => r[1]);
  const mappedKeys = {};
  keys.map((key: string, i: number) => {
    mappedKeys[key] = values[i];
  });
  return mappedKeys;
}

const router = async () => {
  const routes: IRouter[] = [
    { path: '/', view: Home },
    { path: '/detail/:id', view: CarDetail },
    { path: '/404', view: ErrorPage },
  ];

  // Test each route for potential match
  const potentialMatches = routes.map(route => {
    return {
      route: route,
      result: location.pathname.match(pathToRegex(route.path))
    };
  });

  let foundMatch = potentialMatches.find(potentialMatch => potentialMatch.result !== null);
 
  if (!foundMatch) {
    foundMatch = {
      route: routes[routes.length - 1],
      result: [
        location.pathname
      ]
    };
  }
  
  const view = new foundMatch.route.view(getParams(foundMatch));
  
  document.querySelector('#app').innerHTML = await view.render();
  await view.afterRender();
};

export default router;