import { ICar, IDetailsItem, IImagesSizes } from '../../interfaces';
import { detailsTable } from '../../constants';
import { formatCarDetailValue, imageSizes } from '../../utils';
import { imageElement, imageSelectorElement } from './carousel';
import './index.scss'

const carDetailElement = (car: ICar) => {
  const detailWrapper = document.createElement('div');
  detailWrapper.className = 'car-detail__details';

  detailsTable.map((detail: IDetailsItem) => {
    // Loop trough details and create key/value table
    const detailValueElement = document.createElement('div');
    detailValueElement.className = 'car-detail__details__key';
    detailValueElement.innerText = detail.value;

    const detailKeyElement = document.createElement('div');

    // Check if the value needs to be formatted (Currency or Numbers)
    const val = formatCarDetailValue(detail, car[detail.key]);
    
    detailKeyElement.className = 'car-detail__details__value';
    detailKeyElement.innerText = `${detail.prefix || ''}${val}${detail.suffix || ''}`;

    detailWrapper.appendChild(detailValueElement);
    detailWrapper.appendChild(detailKeyElement);
  });

  return detailWrapper;
}

const carAccessoriesElement = (acc: string[]) => {
  const accWrapper = document.createElement('ul');
  accWrapper.className = 'car-detail__accessories';

  // Create a list item for each accessorie
  acc.map(a => {
    const accElement = document.createElement('li');
    accElement.innerText = a;
    accWrapper.appendChild(accElement);
  })
  return accWrapper;
}

export const generateCarDetailBlock = (car: ICar) => {
  const blockElement = document.createElement('div');
  
  // Setup header
  const carTitle = document.createElement('h2');
  carTitle.className = 'car-detail__item__title';
  carTitle.innerText = `${car.brand} ${car.model} (${car.yearOfManufacture})`;
  blockElement.className = "car-detail__item";
  
  const carImagesObj: IImagesSizes = imageSizes(car.images);
  
  // Generate image element
  const carImage = imageElement(carImagesObj);

  // Generate image selector element
  const carImageSelector = imageSelectorElement(carImagesObj);

  // Generate car details element
  const carDetails = carDetailElement(car);

  // Generate car accessoires element
  const carAccElement = carAccessoriesElement(car.accessories);

  const contentElement = document.createElement('div');
  contentElement.className = 'car-detail__item__content';

  // Append title, details and Accessoires to content element
  contentElement.appendChild(carTitle);
  contentElement.appendChild(carDetails);
  contentElement.appendChild(carAccElement);

  // Append image, image selector and content element to wrapper
  blockElement.appendChild(carImage);
  blockElement.appendChild(carImageSelector);
  blockElement.appendChild(contentElement);

  return blockElement;
}