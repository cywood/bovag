import { imageBaseUrl } from '../../constants';
import { IImagesSizes } from '../../interfaces';

// Create image rail for use in the carousel
const imageRail = document.createElement('div');
imageRail.className = 'carousel__rail';
imageRail.setAttribute('style', '--offset: 0');

// The handler for eventlistener
const handleSwitchImage = (offset: number) => {
  imageRail.setAttribute('style', `--offset: ${offset}`)
}

// Content inside the image rail
export const imageElement = (images: IImagesSizes) => {
  const imageWrapper = document.createElement('div');
  imageWrapper.className = 'carousel';

  const imageArr = Object.keys(images);
  const largestImages = images[imageArr[imageArr.length - 1]]; // Get largest images

  largestImages.map((img: string) => {
    // For every image append the image to the rail
    const imageItem = document.createElement('div');
    imageItem.style.backgroundImage = `url(${imageBaseUrl}${img})`;
    imageItem.className = 'carousel__rail__image';
    imageRail.append(imageItem);
  });

  imageWrapper.append(imageRail);
  return imageWrapper;
}

// The image selector element under the main images
export const imageSelectorElement = (images: IImagesSizes) => {
  const selectorWrapper = document.createElement('div');
  selectorWrapper.className = 'car-detail__image-selector';

  // Take the smallest image size for use in the image selector
  const smallestImageKey = Object.keys(images)[0];
  const smallesImages = images[smallestImageKey];

  smallesImages.map((img: string, i: number) => {
    const smallElement = document.createElement('div');
    smallElement.className = 'car-detail__image-selector__image';
    smallElement.style.backgroundImage = `url(${imageBaseUrl}${img})`;

    // Add event listener to update image
    smallElement.addEventListener('click', () => handleSwitchImage(i))
    selectorWrapper.appendChild(smallElement);
  });
  return selectorWrapper;
}