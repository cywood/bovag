import { ICar, IDetailsItem } from '../../interfaces';
import { detailsTable, imageBaseUrl } from '../../constants';
import { formatCarDetailValue } from '../../utils';
import './index.scss'

const imageElement = (image: string) => {
  const img = document.createElement('div');
  img.className = 'cars__item__image';
  img.style.backgroundImage = `url('${imageBaseUrl}${image}')`;
  return img;
}

const carDetailElement = (car: ICar) => {
  const detailWrapper = document.createElement('div');
  detailWrapper.className = 'cars__details';

  detailsTable.map((detail: IDetailsItem) => {
    if (detail.hideSmall) return; // Ignore all keys that are not for the small
    const detailValueElement = document.createElement('div');
    detailValueElement.className = 'cars__details__key';
    detailValueElement.innerText = detail.value;

    const detailKeyElement = document.createElement('div');

    const val = formatCarDetailValue(detail, car[detail.key]);
    
    detailKeyElement.className = 'cars__details__value';
    detailKeyElement.innerText = `${detail.prefix || ''}${val}${detail.suffix || ''}`;

    detailWrapper.appendChild(detailValueElement);
    detailWrapper.appendChild(detailKeyElement);
  });

  return detailWrapper;
}

const carButtonelement = (id: string, title: string) => {
  const ctaWrapper = document.createElement('div');
  ctaWrapper.className = 'cars__item__content__cta';
  const ctaButton = document.createElement('a');
  ctaButton.setAttribute('href', `/detail/${id}`);
  ctaButton.className = 'btn';
  ctaButton.innerText = `Bekijk ${title}`

  ctaWrapper.appendChild(ctaButton);
  return ctaWrapper;
}

export const generateCarBlock = (car: ICar) => {
  const blockElement = document.createElement('div');
  
  // Setup header
  const carTitle = document.createElement('h2');
  carTitle.className = 'cars__item__title';
  carTitle.innerText = `${car.brand} ${car.model} (${car.yearOfManufacture})`;
  blockElement.className = "cars__item";
  
  // Generate image element
  const carImage = imageElement(car.images[0]);

  // Generate car details element
  const carDetails = carDetailElement(car);

  // Generate call to action button
  const carCTA = carButtonelement(car.id, `${car.brand} ${car.model}`);

  const contentElement = document.createElement('div');
  contentElement.className = 'cars__item__content';

  // Append title, details and CTA to content element
  contentElement.appendChild(carTitle);
  contentElement.appendChild(carDetails);
  contentElement.appendChild(carCTA);

  // Append image and content element to wrapper
  blockElement.appendChild(carImage);
  blockElement.appendChild(contentElement);

  return blockElement;
}