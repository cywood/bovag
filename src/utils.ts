import { numberRegex } from "./constants";
import { IDetailsItem, IImagesSizes } from "./interfaces";

export const currencyFormat = (num) => num.toFixed(2).replace('.', ',').replace(numberRegex, '$1.');

// num.toLocaleString() doesnt work properly when you have a different locale
export const formatNumber = (num) => num.toFixed(0).replace(numberRegex, '$1.');



export const formatCarDetailValue = (d: IDetailsItem, value: number) => {
  if (d.isCurrency) {
    return currencyFormat(value);
  }
  if (d.formatNumber) {
    return formatNumber(value);
  }
  return value;
}

export const imageSizes = (images: string[]): IImagesSizes => {
  // Loop trough all images
  return images.reduce((obj, img) => {
    // Capture width (320w) as size, receive groups from exec results
    const {groups} = /.*-(?<size>[0-9]+w)/.exec(img);
    // Check if the size already exist
    const stack = obj[groups.size] || [];
    // Add stack to object
    return {...obj, [groups.size]: [...stack, img]};
  }, {});
}