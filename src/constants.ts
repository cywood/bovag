import { IDetailsItem } from "./interfaces";

export const imageBaseUrl = 'http://localhost:3000';

export const detailsTable: IDetailsItem[] = [
  { key: 'price', value: 'Prijs', prefix: '€ ', isCurrency: true },
  { key: 'mileage', value: 'Kilometerstand', suffix: ' km', formatNumber: true },
  { key: 'fuelType', value: 'Brandstof' },
  { key: 'transmission', value: 'Hand/Automaat' },
  { key: 'gearCount', value: 'Aant. Versnel.' },
  { key: 'bodyType', value: 'Carossorie' },
  { key: 'color', value: 'Kleur', hideSmall: true },
  { key: 'seatCount', value: 'Zittingen', hideSmall: true },
  { key: 'doorCount', value: 'Deuren', hideSmall: true },
  { key: 'yearOfManufacture', value: 'Bouwjaar', hideSmall: true },
]

export const numberRegex = /(\d)(?=(\d{3})+(?!\d))/g;