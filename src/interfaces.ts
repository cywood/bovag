import pageBase from "./pages/page-base";
import pageWrapper from "./pages/page-base";

export interface ICar {
  id: string;
  brand: string;
  model: string;
  price: number;
  mileage: number;
  yearOfManufacture: string;
  fuelType: string;
  transmission: string;
  bodyType: string;
  doorCount: number;
  seatCount: number;
  gearCount: number;
  color: string;
  accessories: string[];
  images: string[];
}

export interface IDetailsItem {
  key: string;
  value: string;
  prefix?: string;
  suffix?: string;
  isCurrency?: boolean;
  formatNumber?: boolean;
  hideSmall?: true;
}


export interface IImagesSizes {
  [key: string]: string[];
}

export interface IRouter {
  path: string;
  view: typeof pageBase;
}